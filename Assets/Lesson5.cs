using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Lesson5 : MonoBehaviour
{
    [SerializeField] Transform      m_holder;
    [SerializeField] TMP_InputField m_inputField;
    [SerializeField] Button         m_btnDemo;
    [SerializeField] GameObject     m_textPrefab;
    List<GameObject>                       m_listObjText = new List<GameObject>();
    private List<int> m_listNumber = new List<int>();

    void Start()
    {
        m_btnDemo.onClick.AddListener(BtnDemoOnClick);
        m_inputField.onEndEdit.AddListener(InputFieldOnChange);
    }

    void OnDestroy()
    {
        m_btnDemo.onClick.RemoveAllListeners();
        m_inputField.onEndEdit.RemoveAllListeners();
    }

    void InputFieldOnChange(string value)
    {
        try
        {
            ClearHolder();
            m_listObjText.Clear();
            string[]  numberStrings = value.Split(",");
            for (int i = 0; i < numberStrings.Length; i++)
            {
                if (int.TryParse(numberStrings[i], out int value1))
                {
                    m_listNumber.Add(value1);
                    var go = Instantiate(m_textPrefab, m_holder);
                    go.transform.localPosition = new Vector3(i * 100, 0, 0);
                    go.GetComponent<TMP_Text>().text = value1.ToString();
                    m_listObjText.Add(go);
                }
                else
                {
                    Debug.LogError("Invalid integer format: " + numberStrings[i]);
                    return;
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Error {e.Message}");
        }
    }


    void BtnDemoOnClick()
    {
        StartCoroutine(SortAndMove());
    }

    IEnumerator SortAndMove()
    {
        for (int i = 0; i < m_listNumber.Count - 1; i++)
        {
            for (int j = 0; j < m_listNumber.Count - 1 - i; j++)
            {
                if (m_listNumber[j] > m_listNumber[j + 1])
                {
                    (m_listNumber[j], m_listNumber[j + 1]) = (m_listNumber[j + 1], m_listNumber[j]);
                    var posA = m_listObjText[j].transform.localPosition;
                    var posB = m_listObjText[j+1].transform.localPosition;
                    yield return StartCoroutine(MoveObject(m_listObjText[j], posA, posB, 0.5f));
                    yield return StartCoroutine(MoveObject(m_listObjText[j + 1], posB, posA, 0.5f));
                    SwapObjects(j, j + 1);
                }
            }
        }
    }
    
    void SwapObjects(int indexA, int indexB)
    {
        (m_listObjText[indexA], m_listObjText[indexB]) = (m_listObjText[indexB], m_listObjText[indexA]);
    }

    IEnumerator MoveObject(GameObject obj, Vector3 startPos, Vector3 endPos, float duration)
    {
        float elapsedTime = 0.0f;
        while (elapsedTime < duration)
        {
            float t = elapsedTime / duration;
            obj.transform.localPosition = Vector3.Lerp(startPos, endPos, t);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        obj.transform.localPosition = endPos;
    }

   
    
    void ClearHolder()
    {
        for (int i = 0; i < m_holder.childCount; i++)
        {
            Destroy(m_holder.transform.GetChild(i).gameObject);
        }
    }
    
    
    
}
