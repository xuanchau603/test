using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Lesson4 : MonoBehaviour
{
    [SerializeField] TMP_InputField m_inputField;
    [SerializeField] TMP_Text       m_txtResult;
    [SerializeField] Button         m_btnChange;
    [SerializeField] Transform      m_holder;
    [SerializeField] Button         m_btnDemo;
    [SerializeField] GameObject     m_textPrefab;
    [SerializeField] Transform m_startPos;
    Stack                           binaryStack = new Stack();
    List<GameObject>                m_listObj      = new List<GameObject>();

    void Start()
    {
        m_btnChange.onClick.AddListener(BtnChangeOnClick);
        m_btnDemo.onClick.AddListener(BtnDemoOnClick);
    }

    void OnDestroy()
    {
        m_btnChange.onClick.RemoveAllListeners();
        m_btnDemo.onClick.RemoveAllListeners();
    }


    void BtnChangeOnClick()
    {
        try
        {
            int  decimalNumber = Int32.Parse(m_inputField.text);
            
            while (decimalNumber > 0)
            {
                int       remainder = decimalNumber % 2;
                binaryStack.Push(remainder);
                var go = Instantiate(m_textPrefab, m_holder);
                go.GetComponent<TMP_Text>().text = remainder.ToString();
                m_listObj.Add(go);
                decimalNumber /= 2;
            }
            m_txtResult.text = "Result: ";
            while (binaryStack.Count > 0)
            {
                m_txtResult.text += binaryStack.Pop();
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Error: {e.Message}");
        }
    }

    void BtnDemoOnClick()
    {
        StartCoroutine(Demo());
    }

    IEnumerator Demo()
    {
        var position = m_startPos.position;
        for (int i = 0; i < m_listObj.Count; i++)
        {
            yield return StartCoroutine(MoveObject(m_listObj[i], m_listObj[i].transform.position, position, 0.5f));
            position.y += 40;
        }
        for (int i = m_listObj.Count - 1; i >= 0; i--)
        {
            yield return StartCoroutine(MoveObject(m_listObj[i], m_listObj[i].transform.position, position, 0.5f));
            position.x += 50;
        }
    }
    
    IEnumerator MoveObject(GameObject obj, Vector3 startPos, Vector3 endPos, float duration)
    {
        float elapsedTime = 0.0f;
        while (elapsedTime < duration)
        {
            float t = elapsedTime / duration;
            obj.transform.position = Vector3.Lerp(startPos, endPos, t);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        obj.transform.position = endPos;
    }
}
