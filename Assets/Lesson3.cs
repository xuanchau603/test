using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Lesson3 : MonoBehaviour
{
    [SerializeField] TMP_InputField m_inputFieldN;
    [SerializeField] TMP_InputField m_inputFieldK;
    [SerializeField] TMP_Text       m_txtResult;
    [SerializeField] Button         m_btnPrintSum;
    [SerializeField] Button         m_btnPrintSequence;

    void Start()
    {
        m_btnPrintSum.onClick.AddListener(BtnPrintSumOnClick);
        m_btnPrintSequence.onClick.AddListener(BtnSequenceOnClick);
    }
    
    void OnDestroy()
    {
        m_btnPrintSum.onClick.RemoveAllListeners();
        m_btnPrintSequence.onClick.AddListener(BtnSequenceOnClick);
    }
    
    void BtnPrintSumOnClick()
    {
        try
        {
            int n = int.Parse(m_inputFieldN.text);
            int k = int.Parse(m_inputFieldK.text);
            if (k <= n)
            {
                m_txtResult.text = $"Total subsequences: {GetSubarrayCount(n, k)}";
            }
            else
            {
                Debug.LogWarning($"K value is incorrect");
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Error: {e.Message}");
        }
    }

    void BtnSequenceOnClick()
    {
        try
        {
            int   n   = int.Parse(m_inputFieldN.text);
            int   k   = int.Parse(m_inputFieldK.text);
            int[] arr = new int[n];
            for (int i = 0; i < n; i++)
            {
                arr[i] = i + 1;
            }
            m_txtResult.text = "";
            GenerateSubsets(arr, k);
        }
        catch (Exception e)
        {
            Debug.Log($"Error: {e.Message}");
        }
    }


    int GetSubarrayCount(int n, int k)
    {
        if (k > n)
            return 0;

        int numerator   = Factorial(n);
        int denominator = Factorial(k) * Factorial(n - k);
        int count       = numerator / denominator;

        return count;
    }

    int Factorial(int number)
    {
        if (number == 0)
            return 1;

        int factorial = 1;
        for (int i = 1; i <= number; i++)
        {
            factorial *= i;
        }

        return factorial;
    }
    
    void GenerateSubsets(int[] arr, int k)
    {
        int[] subset = new int[k];
        GenerateSubsetsHelper(arr, subset, 0, 0, k);
    }

    void GenerateSubsetsHelper(int[] arr, int[] subset, int startIndex, int subsetIndex, int k)
    {
        if (subsetIndex == k)
        {
            PrintSubset(subset);
            return;
        }
        for (int i = startIndex; i < arr.Length; i++)
        {
            subset[subsetIndex] = arr[i];
            GenerateSubsetsHelper(arr, subset, i + 1, subsetIndex + 1, k);
        }
    }

    void PrintSubset(int[] subset)
    {
        foreach (var i in subset)
        {
            m_txtResult.text += $"{i},";
        }
    }
}
