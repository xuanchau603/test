using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Lesson1 : MonoBehaviour
{
    [SerializeField] TMP_InputField m_inputField;
    [SerializeField] TMP_Text       m_txtResult;
    [SerializeField] Button         m_btnCheck;

    void Start()
    {
        m_btnCheck.onClick.AddListener(BtnCheckOnClick);
    }

    void OnDestroy()
    {
        m_btnCheck.onClick.RemoveAllListeners();
    }

    void BtnCheckOnClick()
    {
        try
        {
            int n = Int32.Parse(m_inputField.text);
            m_txtResult.text = IsPerfectSquare(n) ? $"{n} is a perfect square" : $"{n} is not a perfect square";
        }
        catch (Exception e)
        {
            Debug.LogError($"Error: {e.Message}");
        }
    }
    
    bool IsPerfectSquare(int n)
    {
        if (n < 0)
            return false;

        int sqrt = (int)Math.Sqrt(n);
        return sqrt * sqrt == n;
    }
}
