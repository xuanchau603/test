using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Lesson2 : MonoBehaviour
{
    [SerializeField] TMP_InputField m_inputField;
    [SerializeField] TMP_Text       m_txtResult;
    [SerializeField] Button         m_btnCheck;
    
    void Start()
    {
        m_btnCheck.onClick.AddListener(BtnCheckOnClick);
    }

    void OnDestroy()
    {
        m_btnCheck.onClick.RemoveAllListeners();
    }

    void BtnCheckOnClick()
    {
        try
        {
            int n = Int32.Parse(m_inputField.text);
            m_txtResult.text = IsPalindrome(n) ? $"{n} is a palindrome" : $"{n} is not a palindrome";
        }
        catch (Exception e)
        {
            Debug.LogError($"Error: {e.Message}");
        }
    }
    
    public bool IsPalindrome(int n)
    {
        int reverse  = 0;
        int original = n;

        while (n > 0)
        {
            int digit = n % 10;
            reverse =  reverse * 10 + digit;
            n       /= 10;
        }

        return original == reverse;
    }
}
